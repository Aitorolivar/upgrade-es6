//2.1 En base al siguiente javascript, crea variables en base a las propiedades 
// del objeto usando object destructuring e imprimelas por consola. Cuidado, 
// no hace falta hacer destructuring del array, solo del objeto.

const game = {
    title: 'The last us 2', 
    gender: ['action', 'zombie', 'survival'], 
    year: 2020
};

const {title} = game;
// console.log(title);

const {gender} = game;
// console.log(gender);

const {year} = game;
// console.log(year);



// 2.2 En base al siguiente javascript, usa destructuring para crear 3 variables 
// llamadas fruit1, fruit2 y fruit3, con los valores del array. Posteriormente
// imprimelo por consola.

const fruits = ['Banana', 'Strawberry', 'Orange'];

const [fruit1, fruit2, fruit3] = fruits;

// console.log(fruit1);
// console.log(fruit2);
// console.log(fruit3);


// 2.3 En base al siguiente javascript, usa destructuring para crear 2 
// variables igualandolo a la función e imprimiendolo por consola.

const animalFunction = () => {
    return {name: 'Bengal Tiger', race: 'Tiger'}
};

const {name} = animalFunction();
const {race} = animalFunction();
// console.log(name);
// console.log(race);
// console.log(`El nomnre es ${name}, y la raza es ${race}`)

const optionAnimal = animalFunction();

const {name, race} = optionAnimal;

// console.log(`El nomnre es ${name}, y la raza es ${race}`)

// 2.4 En base al siguiente javascript, usa destructuring para crear las 
// variables name e itv con sus respectivos valores. Posteriormente crea 
// 3 variables usando igualmente el destructuring para cada uno de los años 
// y comprueba que todo esta bien imprimiendolo.

const car = {nameCar: 'Mazda 6', itv: [2011, 2015, 2020] }

const {nameCar, itv} = car;

const [ time1, time2, time3 ] = itv;

console.log(`El modelo es ${nameCar}, y ha pasado la itv estos años, ${time1}, ${time2}, ${time3}`)