// 7.1 Dado el siguiente array, haz una suma de todos las notas de los examenes de 
// los alumnos usando la función .reduce().


// const array1 = [1, 2, 3, 4];
// const reducer = (previousValue, currentValue) => previousValue + currentValue;

// // 1 + 2 + 3 + 4
// console.log(array1.reduce(reducer));
// // expected output: 10

// // 5 + 1 + 2 + 3 + 4
// console.log(array1.reduce(reducer, 5));
// // expected output: 15

const exams = [
    {name: 'Yuyu Cabeza Crack', score: 5}, 
    {name: 'Maria Aranda Jimenez', score: 1}, 
    {name: 'Cristóbal Martínez Lorenzo', score: 6}, 
    {name: 'Mercedez Regrera Brito', score: 7},
    {name: 'Pamela Anderson', score: 3},
    {name: 'Enrique Perez Lijó', score: 6},
    {name: 'Pedro Benitez Pacheco', score: 8},
    {name: 'Ayumi Hamasaki', score: 4},
    {name: 'Robert Kiyosaki', score: 2},
    {name: 'Keanu Reeves', score: 10}
];

// const resutl = exams.reduce((acc, cur) =>{
//     return acc+ cur.score;
// },0)

// console.log(resutl);

// 7.2 Dado el mismo array, haz una suma de todos las notas de los examenes de los 
// alumnos que esten aprobados usando la función .reduce().

// const sum = exams.reduce((acc, cur) => {
//     ...acc
//     [acc.score]
// })
// console.log(sum)

const resutl = exams.reduce((acc, exams) => {
    return exams.score >= 5 ? acc + exams.score : acc;
},0)
console.log(resutl)

// 7.3 Dado el mismo array, haz la media de las notas de todos los examenes .reduce().

const media = exams.reduce((acc, exams) => {
    return acc + exams.score
},0)

const totalMedia = media / exams.length

console.log(totalMedia)